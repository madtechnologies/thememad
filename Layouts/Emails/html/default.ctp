<!doctype html>
<html>
	<head>
		<title><?php echo $this->fetch("title")?></title>
		<?php echo $this->fetch("meta")?>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
		<?php echo $this->fetch("css")?>
	</head>
	<body>
		<div class="container">
			<?php echo $this->fetch("content")?>
		</div>
		<?php echo $this->fetch("script")?>
	</body>
</html>
<!doctype html>
<html>
	<head>
		<title><?php echo $this->fetch("title")?></title>
		<?php echo $this->fetch("meta")?>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha256-k2/8zcNbxVIh5mnQ52A0r3a6jAgMGxFJFE2707UxGCk= sha512-ZV9KawG2Legkwp3nAlxLIVFudTauWuBpC10uEafMHYL0Sarrz5A7G79kXh5+5+woxQ5HM559XX2UZjMJ36Wplg==" crossorigin="anonymous">
		<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/superhero/bootstrap.min.css" rel="stylesheet" integrity="sha256-o0IkLyCCWGBI+ryg6bL44/f8s4cb7+5bncR4LvU57a8= sha512-jptu6vg45XTY9uPX3vD5nHN4vASCN2hHl+fhmgkdd/px/bFHKMXmDXhkNmTiCpKqH6sliEPFakl2KZNav2Zo1Q==" crossorigin="anonymous">
		<?php echo $this->fetch("css")?>
	</head>
	<body>
		<div class="container">
			<?php 
			echo $this->Session->flash();
			echo $this->Session->flash("auth");
			?>
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a href="<?php echo Router::url(DS)?>" class="navbar-brand"><?php echo Configure::read("Mad.brandName")?></a>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							<?php if(empty(AuthComponent::user())):?>
							<li>
								<?php echo $this->Html->link("Login", '/login');?>
							</li>
							<li>
								<?php echo $this->Html->link("Register", '/register');?>
							</li>
							<?php else:?>
							<li>
								<?php echo $this->Html->link("Profile", '/profile');?>
							</li>
							<li>
								<?php echo $this->Html->link("Logout", '/logout');?>
							</li>
							<?php endif?>
						</ul>
					</div>
				</div>
			</nav>
			<?php echo $this->fetch("content")?>
		</div>
		<?php echo $this->fetch("script")?>
	</body>
</html>